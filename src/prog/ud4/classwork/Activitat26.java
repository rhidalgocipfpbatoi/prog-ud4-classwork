/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud4.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat26 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduce tu nombre:");
        String nombre = scanner.next().concat(" ");
        System.out.print("Introduce 1 apellido:");
        String apellido1 = scanner.next().concat(" ");
        System.out.print("Introduce 2 apellido:");
        String apellido2 = scanner.next();
        
        String nombreCompleto = nombre.concat(apellido1).concat(apellido2);
        mostrarEnMinusMayusYLongitud(nombreCompleto);
        mostrarOcurrenciasUltimoCaracter(nombreCompleto);
        mostrar2PrimerosCaracteres(nombreCompleto);
        mostrarEnFormatoCamelCase(nombre, apellido1, apellido2);
        mostrarConAsteriscos(nombreCompleto);
        mostrarInvertido(nombreCompleto);
    }
    
    public static void mostrarEnMinusMayusYLongitud(String nombreCompleto) {
        System.out.printf("Minúsculas: %s%n", nombreCompleto.toLowerCase());
        System.out.printf("Mayúsculas: %s%n", nombreCompleto.toUpperCase());
        System.out.printf("Longitud: %d%n", nombreCompleto.length());
    }
    
    public static void mostrarOcurrenciasUltimoCaracter(String nombreCompleto) {
         char ultimoCaracter = nombreCompleto.charAt(nombreCompleto.length()-1);
         
         int ocurrencias = 0;
         for (int i = 0; i < nombreCompleto.length(); i++) {
            if (nombreCompleto.charAt(i) == ultimoCaracter) {
                ocurrencias++;
            }
         }
         
         System.out.printf("Número de ocurrencias de %s es %d%n", 
                 ultimoCaracter, ocurrencias);
        
    }
         
    public static void mostrar2PrimerosCaracteres(String nombreCompleto) {
        
        if (nombreCompleto.length() >= 2) {
            System.out.printf("2 primeros caracteres: %s%n", 
                    nombreCompleto.substring(0, 2));
        }
    }
    
    public static void mostrarEnFormatoCamelCase(
            String nombre, String apellido1, String apellido2) {
        
        String nombreCamelCase = obtenerCadenaEnCamelCase(nombre);
        String apellido1CamelCase = obtenerCadenaEnCamelCase(apellido1);
        String apellido2CamelCase = obtenerCadenaEnCamelCase(apellido2);
        
        System.out.printf("Camelcase: %s %s %s%n", 
                nombreCamelCase, apellido1CamelCase, apellido2CamelCase);
        
        
    }
    
    public static String obtenerCadenaEnCamelCase(String cadena) {
        
        String primerCaracter = cadena.substring(0, 1).toUpperCase();
        String restoCaracteres = cadena.substring(1, cadena.length()).toLowerCase();
        
        return primerCaracter.concat(restoCaracteres);
    }
    
    public static void mostrarConAsteriscos(String nombreCompleto) {
        String asteriscos = "***";
        System.out.printf("Asteriscos: %s%s%s%n", asteriscos, nombreCompleto, asteriscos);
    }
    
    public static void mostrarInvertido(String nombreCompleto) {
        
        for (int i = nombreCompleto.length()-1; i >= 0; i--) {
            System.out.print(nombreCompleto.charAt(i));
        }
        
        System.out.println("");
    }
    
}
