/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud4.classwork;

/**
 *
 * @author batoi
 */
public class Activitat22 {
    
    public static void main(String[] args) {
        separarLetras("Hola Mon");
    }
    
    public static void separarLetras(String texto) {
        
        /*for (int i = 0; i < texto.length(); i++) {
            char caracter = texto.charAt(i);
            System.out.print(caracter);
            
            if (i < texto.length()-1) {
                System.out.print("-");
            }
        }*/
        
        for (int i = 0; i < texto.length()-1; i++) {
            char caracter = texto.charAt(i);
            
            if (caracter != ' '){
                System.out.print(caracter + "-");
            }
        }
        
        System.out.println(texto.charAt(texto.length()-1));
    }
}
