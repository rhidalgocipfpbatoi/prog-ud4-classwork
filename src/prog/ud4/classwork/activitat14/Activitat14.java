/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud4.classwork.activitat14;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat14 {
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        
        while(true) {
            System.out.println("Introduce un número:");
            int numero = scanner.nextInt();
            
            if (Matematica.esPrimo(numero)) {
                System.out.printf("El número %d es primo\n", numero);
            } else{
                System.out.printf("El número %d no es primo\n", numero);
            }
        }
    }
}
