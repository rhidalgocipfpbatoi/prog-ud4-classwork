/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud4.classwork.activitat14;

/**
 *
 * @author batoi
 */
public class Matematica {
    
    public static void main(String[] args) {
        System.out.println(esPrimo(4));
        System.out.println(esPrimo(2));
        System.out.println(esPrimo(3));
        System.out.println(esPrimo(-12));
        System.out.println(esPrimo(0));
        System.out.println(esPrimo(1));
    }
    
    public static boolean esPrimo(long numero) {
        
        if (numero <= 1) {
            return false;
        }
        
        for (int i = 2; i < numero; i++) {
            if (numero % i == 0) {
                return false;
            } 
        }
        
        return true;
    }
    
}
