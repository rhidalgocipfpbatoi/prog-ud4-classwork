/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud4.classwork;

/**
 *
 * @author batoi
 */

public class Activitat12 {

    public static void main(String[] args) {

        imprimePiramide('x', 10);
    }

    public static void imprimePiramide(char caracter, int numLineas) {
        for (int nLinea = 1; nLinea <= numLineas; nLinea++) {
           
            int cantitatEspaisBlanc = numLineas - nLinea;
            Activitat10.imprime(' ', cantitatEspaisBlanc);
           
            int cantitatCaracters = nLinea * 2 - 1;
            Activitat10.imprime(caracter, cantitatCaracters);
           
            System.out.println();
        }
    }
}
    
