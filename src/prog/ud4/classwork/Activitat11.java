
package prog.ud4.classwork;

/**
 *
 * @author batoi
 */
public class Activitat11 {
    
    public static void main(String[] args) {
        imprime('?', 6, 3);
        imprime('=', 2, 9);
    }
    
    public static void imprime(char caracter, int numVeces, int numLineas) {
        
        for (int i = 0; i < numLineas; i++) {
            Activitat10.imprime(caracter, numVeces);
        }
    }
    
    
}
