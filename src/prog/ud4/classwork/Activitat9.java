
package prog.ud4.classwork;

/**
 *
 * @author batoi
 */
public class Activitat9 {
    
    public static void main(String[] args) {
        int mayor = maximoDe4Numeros(18, -2, 4, 5);
        System.out.printf("El mayor es %d\n", mayor);
    }
    
    public static int maximoDe4Numeros(
            int num1, int num2, int num3, int num4) {
     
        int maximo = Activitat6.maximoDe3Numeros(num1, num2, num3);
        return Math.max(maximo, num4);
    }
}
