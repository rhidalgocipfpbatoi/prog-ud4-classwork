/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud4.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat4 {
    
    public static void main(String[] args) {
        int maximo = maximoDe3Numeros();
        System.out.printf("El mayor es %d\n", maximo);
    }
    
    public static int maximoDe3Numeros() {
        
        Scanner scanner = new Scanner(System.in);
        
        int mayor = Integer.MIN_VALUE;
        
        for (int i = 1; i <= 3; i++) {
            System.out.printf("Introduce número %d:", i);
            int numero = scanner.nextInt();
            if (numero > mayor) {
                mayor = numero;
            }
        }
        
        return mayor;
        
    }
    
}
