
package prog.ud4.classwork;

/**
 *
 * @author batoi
 */
public class Activitat10 {
    
    public static void main(String[] args) {
        imprime('z', 40);
        System.out.println();
        imprime('7', 2);
        System.out.println();
        imprime('.', 54);
        System.out.println();
    }
    
    public static void imprime(char caracter, int numVeces) {
        
        for (int i = 0; i < numVeces; i++) {
            System.out.print(caracter);
        }
    }
}
