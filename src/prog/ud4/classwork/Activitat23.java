/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud4.classwork;

/**
 *
 * @author batoi
 */
public class Activitat23 {
    
    public static void main(String[] args) {
        System.out.println(mitadDeCadena("Me gusta correr"));
        System.out.println(mitadDeCadena("Hola qué tal"));
        
    }
    
    public static String mitadDeCadena(String cadena) {
        
        return cadena.substring(0, cadena.length()/2);
    }
}
